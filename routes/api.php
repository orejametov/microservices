<?php

use App\Http\Controllers\GrpcController;
use App\Http\Controllers\HttpController;
use App\Http\Controllers\RestApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('grpc/settings', [GrpcController::class, 'index']);
Route::post('grpc/settings', [GrpcController::class, 'store']);

Route::get('rest-api/settings', [RestApiController::class, 'index']);
Route::post('rest-api/settings', [RestApiController::class, 'store']);

Route::get('http/settings', [HttpController::class, 'index']);
Route::post('http/settings', [HttpController::class, 'store']);
