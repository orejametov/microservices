<?php


namespace App\Services\Grpc;


use App\Http\Requests\Services\Grpc\Form;
use App\Services\IMicroServices;
use Illuminate\Support\Facades\Validator;

class GrpcMicroServices implements IMicroServices
{
    public function index()
    {
        $data = [];//"получание сервиса из ГРПС микросервиса"
        $this->validate($data);
        return $data;
    }

    public function store(array $settings)
    {
        // сохранение через ГРПС сервис
        return $settings;
    }

    private function validate(array $data)
    {
        Validator::make($data, (new Form())->rules());
    }
}
