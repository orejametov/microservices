<?php


namespace App\Services\RestApi;


use App\Http\Requests\Services\RestApi\Form;
use App\Services\IMicroServices;
use Illuminate\Support\Facades\Validator;

class RestApiMicroServices implements IMicroServices
{
    public function index()
    {
        $data = [];//"получание сервиса из РестАпи микросервиса"
        $this->validate($data);
        return $data;
    }

    public function store(array $settings)
    {
        // сохранение через РестАпи сервис
        return $settings;
    }

    private function validate(array $data)
    {
        Validator::make($data, (new Form())->rules());
    }
}
