<?php


namespace App\Services;


use Illuminate\Http\Request;

interface IMicroServices
{
    public function index();//read data
    public function store(array $settings);//insert data
}
