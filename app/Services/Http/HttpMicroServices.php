<?php


namespace App\Services\Http;


use App\Http\Requests\Services\Http\Form;
use App\Services\IMicroServices;
use Illuminate\Support\Facades\Validator;

class HttpMicroServices implements IMicroServices
{
    public function index()
    {
        $data = [];//"получание сервиса из Хттп микросервиса"
        $this->validate($data);
        return $data;
    }

    public function store(array $settings)
    {
        // сохранение через Хттп сервис
        return $settings;
    }

    private function validate(array $data)
    {
        Validator::make($data, (new Form())->rules());
    }
}
