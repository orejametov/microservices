<?php


namespace App\Http\Controllers;


use App\Http\Requests\Services\Grpc\Form;
use App\Services\Grpc\GrpcMicroServices;
use Illuminate\Support\Facades\Validator;

class GrpcController extends Controller
{
    private $grpcMicroServices;

    public function __construct(GrpcMicroServices $grpcMicroServices)
    {
        $this->grpcMicroServices = $grpcMicroServices;
    }

    public function index()
    {
        return $this->grpcMicroServices->index();
    }

    public function store(Form $request)
    {
        return $this->grpcMicroServices->store($request->validated());
    }
}
