<?php


namespace App\Http\Controllers;


use App\Http\Requests\Services\RestApi\Form;
use App\Services\IMicroServices;
use App\Services\RestApi\RestApiMicroServices;
use Illuminate\Support\Facades\Validator;

class RestApiController extends Controller
{
    private $restApiMicroServices;

    public function __construct(RestApiMicroServices $restApiMicroServices)
    {
        $this->restApiMicroServices = $restApiMicroServices;
    }

    public function index()
    {
        return $this->restApiMicroServices->index();
    }

    public function store(Form $request)
    {
        return $this->restApiMicroServices->store($request->validated());
    }
}
