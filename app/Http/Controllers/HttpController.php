<?php


namespace App\Http\Controllers;


use App\Http\Requests\Services\Http\Form;
use App\Services\Http\HttpMicroServices;
use App\Services\IMicroServices;
use Illuminate\Support\Facades\Validator;

class HttpController extends Controller
{
    private $httpMicroServices;

    public function __construct(HttpMicroServices $httpMicroServices)
    {
        $this->httpMicroServices = $httpMicroServices;
    }

    public function index()
    {
        return $this->httpMicroServices->index();
    }

    public function store(Form $request)
    {
        return $this->httpMicroServices->store($request->validated());
    }
}
