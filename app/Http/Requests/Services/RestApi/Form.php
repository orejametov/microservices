<?php

namespace App\Http\Requests\Services\RestApi;

use Illuminate\Foundation\Http\FormRequest;

class Form extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'field1' => 'string|required',
            'field2' => 'boolean|required',
            'field3' => 'array|required'
        ];
    }
}
